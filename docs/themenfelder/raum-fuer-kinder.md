# Raum für Kinder

* Hintere Insel gesperrt
* Insel-Spielplatz gesperrt
* Grünfläche beim Hafenbrunnen wird bepflanzt
* Zugang beim Casino gesperrt
* Klettern in der Oskar-Groll-Anlage verboten
* Rampe im Segelhafen
* Hunde und Dreck an der Gerberschanze
* Systemrelevante Kindergartenkinder an der Gerberschanze
